# Project: Sunflower ebook/book repository

## How to use

Make sure you have `make` and python3 with lxml installed. You also need `java` to be able to validate the ebook with [EpubCheck](https://github.com/idpf/epubcheck), `inotifywait` from inotify-tools and [Calibre](http://calibre-ebook.com/) for the commands `ebook-polish` and `ebook-view`. Kindle export requires ImageMagick. PDF export requires `lualatex` from TeX Live, and some of its extra packages and GraphicsMagick for cover image generation. Some python modules are required for html2latex.
 - Copy the epub ebook and put it next to the Makefile and rename it to `current.epub`. This is a hardcoded name that the tool uses to read the book from.
 - Open a terminal and run `make extractcurrent`. This will populate `src/` with the contents of the ebook using `unzip`.
 - You can now validate the ebook. Run `make validate`. It first builds the epub into `build/` based on the contents of `src/` (same as `make build`). Then it downloads EpubCheck (only once) and runs it on the ebook.
 - You can open the built ebook in Calibre's viewer with `make view`.
 - You can export a Kindle ebook by running `make buildkindle`. This will on first run download [KindleGen](http://www.amazon.com/gp/feature.html?docId=1000765211) (Linux version only). KindleGen might output some helpful warnings on how you can improve the epub file. By default the Makefile uses KindleGen's `-c1` compression, but you can change that if you want.
 - Open up `current.epub` in your epub editor. Run `make watchcurrent`. The tool will now wait until you save the file. When you do it will run the validator on it. It's quite useful to spot errors or warnings as you edit the epub. When you're done, run `make extractcurrent` and the `src/` is updated. This will overwrite everything in `src/`.
 - You can run `make publish` to validate, build for kobo/kindle and copy them to `release/` with today's date in filename.
 - Run `make buildbook` to build the PDF. Modify `book.tex` to define pages and chapters to include, and `html2latex-config.py` to configure the styles of the generated book. The `novel` TeX documentclass is included.
 - Run `make bookcover` to convert `cover.png` into a PDF version of the cover. Modify `cover.tex` to set the dimensions of the image, trim size and set PDF title/author.

This repo is based on [epubmake](https://github.com/daniel-j/epubmake) with added support for PDF (using LaTeX).

## buildbook dependencies

Arch Linux: `pacman -S texlive-latexextra python-cssselect python-cssutils python-lxml`
