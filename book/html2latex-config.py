#!/usr/bin/env python3

# Configure html2latex

import re, os, sys
sys.path.append('tools/html2latex')
from html2latex import Html2Latex

replacements_head = {}
replacements_tail = {}

# Available options: hyperref, footnotes or None
hyperlinks = None


def s(start='', end='', ignoreStyle=False, ignoreContent=False):
    # helper for generating the selector objects
    return {
        'start': start,
        'end': end,
        'ignoreStyle': ignoreStyle,
        'ignoreContent': ignoreContent
    }


def handle_anchor(selector, el):
    href = el.get('href')
    name = el.get('name')

    if hyperlinks == 'hyperref':
        start = ''
        end = ''
        if href and href.startswith('#'):
            start = '\\hyperlink{' + href[1:] + '}{'
            end = '}'
        elif name:
            start = '\\hypertarget{' + name + '}{'
            end = '}'
        elif href:
            start = '\\href{' + href + '}{'
            end = '}'
        return s(start, end)
    elif hyperlinks == 'footnotes':
        if href and not href.startswith('#'):
            return s(end='\\footnote{' + href + '}')
    return None


def handle_paragraph(selector, el):
    # hanging indentation
    match = r'^“'
    node = el
    while True:
        if node.text and re.match(match, node.text):
            replacements_head[node] = (match, '\\\\hanging{“}')
            break
        elif not node.text and node.__len__() > 0:
            node = node[0]
        else:
            break

    return s('\n\n')


def handle_paragraph_double(selector, el):
    res = handle_paragraph(selector, el)
    return s('')


def handle_nbsp(el, pos, char):
    if el.get('class') == 'chapter-number':
        return '~\\,'
    return '~'


def handle_img(selector, el):
    name = os.path.basename(el.get('src'))
    name = name.replace('-bw.jpg', '-bw.png')
    args = ""
    if el.get('class') == "chapterart":
        args = "[ht,0,-0.3\\nbs]"
    return s('\n\\FloatImage' + args + '{' + os.path.join('build/artwork', name) + '}\n')

def handle_chapterstart(selector, el):
    lines = el.get('data-lines', None)
    style = el.get('data-style', None)
    if lines is not None:
        lines = "[" + str(int(lines)) + "]"
    elif style is not None:
        lines = "[*]"
    else:
        lines = ""

    if style is not None:
        style = "[" + style + "]"
    else:
        style = ""
    return s('\n\\begin{ChapterStart}' + lines + style + '\n', '\n\\end{ChapterStart}\n')

# Available options: hyperref, footnotes or None
hyperlinks = None

selectors = {
    # defaults
    # 'html': s('\\thispagestyle{empty}\n{\n', '\n}\n'),
    # 'html': s('{', '}\n'),
    'head': s(ignoreContent=True, ignoreStyle=True),
    # 'body': s('', ''),
    'blockquote': s('\n\\begin{quotation}', '\n\\end{quotation}'),
    'ol': s('\n\\begin{enumerate}', '\n\\end{enumerate}'),
    'ul': s('\n\\begin{itemize}', '\n\\end{itemize}'),
    'li': s('\n\t\item '),
    'i': s('\\textit{', '}', ignoreStyle=True),
    'b, strong': s('\\textbf{', '}', ignoreStyle=True),
    'em': s('\\emph{', '}', ignoreStyle=True),
    'u': s('\\underline{', '}', ignoreStyle=True),
    'sub': s('\\textsubscript{', '}'),
    'sup': s('\\textsuperscript{', '}'),
    'br': s('\\\\\n'),
    'hr': s('\n\n\\sceneline\n', ignoreStyle=True),
    'a': handle_anchor,

    # customized
    'p': handle_paragraph,
    '.chapterstart': handle_chapterstart,
    '.chapter-number': s('\n\\mychapternumber{', '}\n', ignoreStyle=True),
    '.chapter-name': s('\n\\mychaptername{', '}\n', ignoreStyle=True),
    '#toc h1, #authorsnote h1': s('\n\n{\\centering\\charscale[1.25]{', '}\\par}\n\\null\n\\vspace*{\\nbs}', ignoreStyle=True),
    '.no-break br': s('\\\\*\n'),
    '.break': s('\n\n\scenebreakone', ignoreStyle=True, ignoreContent=True),
    '.break.style2': s('\n\n\scenebreaktwo', ignoreStyle=True, ignoreContent=True),
    '.center': s('\n\n{\\csname @flushglue\\endcsname=0pt plus .25\\textwidth\n\\noindent\\centering{}', '\\par\n}', ignoreStyle=True),
    '.djazz': s('djazz', '', ignoreContent=True),
    '.vfill': s('\n\n\\vspace*{\\fill}', '', ignoreContent=True, ignoreStyle=True),
    'img': handle_img
}

characters = {
    u'\u00A0': handle_nbsp, # &nbsp;
    u'\u2009': '\\,', # &thinsp;
    u'\u2003': '\hspace*{1em}', # &emsp;
    '[': '{[}',
    ']': '{]}'
}

styles = {
    'page-break-inside': {
        'avoid': ('\\begin{samepage}', '\\end{samepage}')
    },
    'page-break-before': {
        'avoid': ('\\nopagebreak{}', '')
    },
    # defaults
    'font-weight': {
        'bold': ('\\textbf{', '}'),
        'bolder': ('\\textbf{', '}')
    },
    'font-style': {
        'italic': ('\\textit{', '}')
    },
    'font-variant': {
        'small-caps': ('\\textsc{', '}')
    },
    'text-indent': {
        '0': ('\\noindent{}', ''),
        '-1em': ('\\noindent\\hspace*{-1em}', ''),
        '1em': ('\\indent{}','')
    },
    'text-align': {
        'left': ('{\\raggedright{}', '}'),
        'center': ('{\\centering{}', '\\par}'),
        'right': ('{\\raggedleft{}', '}')
    },
    'text-wrap': {
        # 'balance': ('{\\csname @flushglue\\endcsname=0pt plus .25\\textwidth\n', '\n}')
    },
    '-latex-needspace': {
        '2': ('\n\n\\needspace{2\\baselineskip}\n', '')
    },

    'display': {
        'none': s(ignoreContent=True, ignoreStyle=True)
    },

    # customized
    'margin': {
        '0 2em': ('\n\n\\begin{adjustwidth}{2em}{1em}\n', '\n\\end{adjustwidth}\n\n'),
        '0 1em 0 2em': ('\n\n\\begin{adjustwidth}{2em}{1em}\n', '\n\\end{adjustwidth}\n\n'),
        '0 1em': ('\n\n\\begin{adjustwidth}{1em}{1em}\n', '\n\end{adjustwidth}\n\n') # only used on introduction page
    },
    'margin-top': {
        '1em': ('\\null\\par\\noindent{}', '')
    },
    'margin-bottom': {
        '1em': ('', '\\null\\par\n')
    },
    'font-size': {
        '1.2em': ('\\charscale[1.2]{', '}')
    },
    'font-feature-settings': {
        '"smcp" on': ('\\textsc{', '}')
    },
    '-latex-display': {
        'none': s(ignoreContent=True, ignoreStyle=True)
    }
}


html2latex = Html2Latex(
    selectors=selectors,
    characters=characters,
    styles=styles,
    replacements_head=replacements_head,
    replacements_tail=replacements_tail
).parse_args().run()
